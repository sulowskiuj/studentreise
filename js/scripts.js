var $omSlider;
var $sammarbeidSlider;
var $omSly;
var $sammarbeidSly;
var $hvorforSlider;
var $hvorforSly;

$(function () {

    checkMenu();

    //menu scrollTo
    $("nav ul a, #mobileNav ul a").click(function (e) {

        e.preventDefault();
        var target = $(this).attr("href");
        var top;

        if ($(target).height() > $(window).height()) {
            top = $(target).offset().top;
        }
        else {
            top = $(target).offset().top - ($(window).height() - $(target).outerHeight()) / 2;
        }

        $("html, body").animate({
            scrollTop: top
        }, 1000);

        $("#mobileNavWrap").removeClass("expanded");//hide #mobileNav
        $("#mobileNav .showMenu p").html("menu");

    });

    //mobileNav toggle
    $("#mobileNav .showMenu").click(function () {
        if ($("#mobileNavWrap").hasClass("expanded")) {
            $("#mobileNavWrap").removeClass("expanded");
            $("#mobileNav .showMenu p").html("menu");
        } else {
            $("#mobileNavWrap").addClass("expanded");
            $("#mobileNav .showMenu p").html("lukke");
        }
    });

    $("nav .jegErMed").click(function (e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: $("#turene").offset().top
        }, 1000);
    });

    //scroll button
    $(".scroll").click(function () {
        $("html, body").animate({
            scrollTop: $("#turene").offset().top
        }, 1000);
    });

    //#turene .element hover
    $("#turene .element").mouseover(function () {
        //$(this).find(".button").css('display', 'block');
        $(this).find(".button").css('bottom', '-35px');
    });
    $("#turene .element").mouseleave(function () {
        $(this).find(".button").css('bottom', '-160px');
        //$(this).find(".button").css('display','block');

    });

    //setting .wrapElement width in #omSlider and #sammarbeidSlider
    if ($(window).width() > 559) {
        $(".wrapElement").css("width", $(window).width() / 2);
        $("#omSlider").css("width", $(window).width() / 2);
        $("#sammarbeidSlider").css("width", $(window).width() / 2);
    } else {
        $(".wrapElement").css("width", $(window).width());
        $("#omSlider").css("width", $(window).width());
        $("#sammarbeidSlider").css("width", $(window).width());
    }

    //omSlider
    $omSlider = $('#omSlider');
    var $wrap = $omSlider.parent();
    $omSly = new Sly('#omSlider',
            {
                horizontal: 1,
                itemNav: 'forceCentered',
                smart: 1,
                //scrollBy: 1,
                mouseDragging: 1,
                touchDragging: 1,
                releaseSwing: 1,
                pagesBar: $wrap.find('.pages'),
                activatePageOn: 'click',
                speed: 300,
                elasticBounds: 1,
                activateMiddle: true
            }
    );

    $omSly.on('active',
            function (eventName, itemIndex) {
                var index = itemIndex + 1;
                $('#om .left div').css('opacity', '0');
                $('#om .left div:nth-of-type(' + index + ')').css('opacity', '1');
            });
    $omSly.init();
    //-----------------------------------------------------

    //sammarbeidSlider
    $sammarbeidSlider = $('#sammarbeidSlider');
    var $wrap = $sammarbeidSlider.parent();

    $sammarbeidSly = new Sly($sammarbeidSlider, {
        horizontal: 1,
        itemNav: 'basic',
        smart: 1,
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        pagesBar: $wrap.find('.pages'),
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
    }).init();
    //----------------------------------------------------------

    //hvorforSlider
    $hvorforSlider = $('#hvorforSlider');
    var $wrap = $hvorforSlider.parent();

    $hvorforSly = new Sly($hvorforSlider, {
        horizontal: 0,
        itemNav: 'forceCentered',
        smart: 1,
        mouseDragging: 1,
        touchDragging: 1,
        releaseSwing: 1,
        pagesBar: $wrap.find('.pages'),
        activatePageOn: 'click',
        speed: 300,
        elasticBounds: 1,
        activateMiddle: true
    });

    $hvorforSly.on('active',
            function (eventName, itemIndex) {
                var index = itemIndex + 1;
                $('#hvorfor .right div').css('opacity', '0');
                $('#hvorfor .right div:nth-of-type(' + index + ')').css('opacity', '1');
                $('#hvorfor .icoHolder div').css('opacity', '0');
                $('#hvorfor .icoHolder div:nth-of-type(' + index + ')').css('opacity', '1');
            });
    $hvorforSly.init();

});

//baloons movement
$(window).mousemove(function (e) {

    var x = Math.floor(e.pageX - $(window).width() / 2);
    var y = Math.floor(e.pageY - $(window).scrollTop() - $(window).height() / 2);
    $b1 = $(".baloon1");
    $b2 = $(".baloon2");
    $b3 = $(".baloon3");
    $b4 = $(".baloon4");
    $b5 = $(".baloon5");

    $b1.css("top", 60 + y / 35);
    $b1.css("right", 60 + x / 55);

    $b2.css("top", -50 + y / 30);
    $b2.css("left", -60 + x / 50);

    $b3.css("bottom", -100 + y / 25);
    $b3.css("right", -200 + x / 45);

    $b4.css("top", -190 + y / 30);
    $b4.css("left", -90 + x / 50);

    $b5.css("bottom", -70 + y / 25);
    $b5.css("right", -70 + x / 45);

});

$(window).resize(function () {

    if ($(window).width() > 559) {
        $(".wrapElement").css("width", $(window).width() / 2);
        $("#omSlider").css("width", $(window).width() / 2);
        $("#sammarbeidSlider").css("width", $(window).width() / 2);
    } else {
        $(".wrapElement").css("width", $(window).width());
        $("#omSlider").css("width", $(window).width());
        $("#sammarbeidSlider").css("width", $(window).width());
    }

    if ($("#omSlider").length > 0) {
        $omSly.reload();
    }

    if ($("#hvorforSlider").length > 0) {
        $hvorforSly.reload();
    }

    if ($("#sammarbeidSlider").length > 0) {
        $sammarbeidSly.reload();
    }
});

$(window).scroll(function () {
    checkMenu();
});

function checkMenu() {
    var active = "";
    $("#turene, #om, #hvorfor, #sammarbeid, #kontakt").each(function () {
        if ($(this).offset().top < $(document).scrollTop() + $(window).height() / 2)
            active = $(this).attr("id");
    });

    //show .loadMore in nav on #turene.active

    if ($(window).scrollTop() + $(window).height() - 200 > $("#turene").offset().top && $(window).scrollTop() + $(window).height() / 2 + 330 < $("#turene").offset().top + $("#turene").outerHeight()) {
        $("nav .loadMore").fadeIn();
        $("nav .jegErMed").fadeOut();
        $("nav").css("height", "490px");
    } else {
        $("nav .loadMore").fadeOut();
        $("nav .jegErMed").fadeIn();
        $("nav").css("height", "530px");
    }

    //---------------------------------------

    $("nav a").removeClass("active");
    if (active.length > 0) {
        $("nav a[href=#" + active + "]").addClass("active");
    }
}
    